<?php

namespace Spatie\MediaLibrary\MediaCollections\Models\Pivot;

use Illuminate\Database\Eloquent\Relations\MorphPivot;
use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediableCollection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Mediable extends MorphPivot
{
    protected $table = 'mediables';

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    public function related()
    {
        return $this->morphTo(__FUNCTION__, 'mediable_type', 'mediable_id');
    }

    public function newCollection(array $models = [])
    {
        return new MediableCollection($models);
    }
}
