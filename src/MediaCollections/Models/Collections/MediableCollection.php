<?php

namespace Spatie\MediaLibrary\MediaCollections\Models\Collections;

use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Database\Eloquent\Collection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\MediaCollections\Models\Pivot\Mediable;

class MediableCollection extends Collection
{
    public function toMedias()
    {
        return $this->map(function(Mediable $mediable){
            return $mediable->media;
        });
    }

    public function toMediables()
    {
        return $this->map(function(Mediable $mediable){
            return $mediable->mediable;
        });
    }
}
